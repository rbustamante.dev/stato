// import { useSyncExternalStoreWithSelector } from 'use-sync-external-store/shim/with-selector'
// Como use-sync-external-store solo expone CJS, solución alternativa hasta que ESM sea compatible.
import useSyncExternalStoreExports from "use-sync-external-store/shim/with-selector";
const { useSyncExternalStoreWithSelector } = useSyncExternalStoreExports;
import { StoreApi, SliceCreator, WithReact, ExtractState, UseInternalState, UseInternalStateCreator, UseStoreCreator } from "./entity";

const useInternalStateCreator: UseInternalStateCreator = (createState) => {
  type TState = ReturnType<typeof createState>;
  type Listener = (state: TState, prevState: TState) => void;
  let state: TState;
  const listeners: Set<Listener> = new Set();
  const setState: StoreApi<TState>["setState"] = (partial, preserve) => {
    const nextState = typeof partial === "function" ? (partial as (state: TState) => TState)(state) : partial;
    if (!Object.is(nextState, state)) {
      const prevState = state;
      state = (nextState as TState);
      (typeof preserve !== "undefined" && typeof preserve.save === "string") ? sessionStorage.setItem(preserve.save, JSON.stringify(state)) : null;
      listeners.forEach((listener) => { return listener(state, prevState); });
    }
  };
  const getState: StoreApi<TState>["getState"] = () => {
    return state;
  };
  const subscribe: StoreApi<TState>["subscribe"] = (listener) => {
    listeners.add(listener);
    return () => { return listeners.delete(listener); };
  };
  const api = { setState, getState, subscribe };
  state = createState(setState, getState, api);
  return api as any;
};

const useInternalState = ((createState) => { return createState ? useInternalStateCreator(createState) : useInternalStateCreator; }) as UseInternalState;

function useInternalStore<S extends WithReact<StoreApi<unknown>>>(api: S): ExtractState<S>;

function useInternalStore<S extends WithReact<StoreApi<unknown>>, U>(api: S, selector: (state: ExtractState<S>) => U): U;

function useInternalStore<TState, StateSlice>(api: WithReact<StoreApi<TState>>, selector: (state: TState) => StateSlice = api.getState as any) {
  const slice = useSyncExternalStoreWithSelector(api.subscribe, api.getState, api.getServerState || api.getState, selector);
  return slice;
}

const useInternalStoreCreator = <T>(createState: SliceCreator<T>) => {
  const api = typeof createState === "function" ? useInternalState(createState) : createState;
  const useInternalStoreSelector: any = (selector?: any) => { return useInternalStore(api, selector); };
  Object.assign(useInternalStoreSelector, api);
  return useInternalStoreSelector;
};

export const useStoreCreator = (<T>(createState: SliceCreator<T> | undefined) => { return createState ? useInternalStoreCreator(createState) : useInternalStoreCreator; }) as UseStoreCreator;
