export type SetStateInternal<T> = {
    _(
        partial: T | Partial<T> | { _(state: T): T | Partial<T> }["_"],
        preserve?: { save: string }
    ): void
}["_"]

export type StoreApi<T> = {
    setState: SetStateInternal<T>
    getState: () => T
    subscribe: (listener: (state: T, prevState: T) => void) => () => void
}

export type Get<T, K, F> = K extends keyof T ? T[K] : F

export type Mutate<S, Ms> = number extends Ms["length" & keyof Ms]
    ? S
    : Ms extends []
    ? S
    : Ms extends [[infer Mi, infer Ma], ...infer Mrs]
    ? Mutate<StoreMutators<S, Ma>[Mi & StoreMutatorIdentifier], Mrs>
    : never

export type SliceCreator<
    T,
    Mis extends [StoreMutatorIdentifier, unknown][] = [],
    Mos extends [StoreMutatorIdentifier, unknown][] = [],
    U = T
> = ((
    setState: Get<Mutate<StoreApi<T>, Mis>, "setState", never>,
    getState: Get<Mutate<StoreApi<T>, Mis>, "getState", never>,
    store: Mutate<StoreApi<T>, Mis>
) => U) & { $$storeMutators?: Mos }

export type StoreMutators<S, A> = unknown
export type StoreMutatorIdentifier = keyof StoreMutators<unknown, unknown>

export type UseInternalState = {
    <T, Mos extends [StoreMutatorIdentifier, unknown][] = []>(
        initializer: SliceCreator<T, [], Mos>
    ): Mutate<StoreApi<T>, Mos>

    <T>(): <Mos extends [StoreMutatorIdentifier, unknown][] = []>(
        initializer: SliceCreator<T, [], Mos>
    ) => Mutate<StoreApi<T>, Mos>
}

export type UseInternalStateCreator = <T, Mos extends [StoreMutatorIdentifier, unknown][] = []>(
    initializer: SliceCreator<T, [], Mos>
) => Mutate<StoreApi<T>, Mos>

export type ExtractState<S> = S extends { getState: () => infer T } ? T : never

export type WithReact<S extends StoreApi<unknown>> = S & { getServerState?: () => ExtractState<S> }

export type UseInternalStoreSelector<S extends WithReact<StoreApi<unknown>>> = {
    (): ExtractState<S>
    <U>(selector: (state: ExtractState<S>) => U): U
} & S

export type UseStoreCreator = {
    <T, Mos extends [StoreMutatorIdentifier, unknown][] = []>(
        initializer: SliceCreator<T, [], Mos>
    ): UseInternalStoreSelector<Mutate<StoreApi<T>, Mos>>
    <T>(): <Mos extends [StoreMutatorIdentifier, unknown][] = []>(
        initializer: SliceCreator<T, [], Mos>
    ) => UseInternalStoreSelector<Mutate<StoreApi<T>, Mos>>
}