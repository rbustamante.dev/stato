import { defineConfig } from 'vite';
import { resolve } from 'path';
import react from '@vitejs/plugin-react';
import dts from "vite-plugin-dts";

export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, "index.ts"),
      name: "stato",
      formats: ["es", "umd"],
      fileName: (format) => `index.${format}.js`,
    },
    emptyOutDir: true,
    manifest: false,
    outDir: './build',
    rollupOptions: {
      external: ["react", "react-dom"],
      output: {
        sourcemap: false,
        assetFileNames: "index.[ext]",
        globals: {
          react: "React", "react-dom": "ReactDOM",
        }
      }
    },
    reportCompressedSize: false,
    chunkSizeWarningLimit: 500,
  },
  publicDir: false,
  clearScreen: false,
  plugins: [react(), dts()],
})
