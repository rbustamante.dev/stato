## STATO

This library was built with TypeScript to handle states in React applications.

It is based on a simplified version of the Zustand core. Compatible with React +18.

In order to use the library a store must be declared.

```javascript
import { useStoreCreator } from "leastore"

export const useStore = useStoreCreator<SLICE_TYPE>((...state) => {
    return {
        ...testSlice(...state)
    }
})
```

Then it is necessary to declare at least one slice to compose the state.

```javascript
import { SliceCreator } from "leastore";

export const testSlice: SliceCreator<SLICE_TYPE> = (setState, getState) => {
    return {
        testState: {
            name: "name",
            setName: () => {
                setState((state) => {
                    return {
                        testState: { ...state.testState, name: "new name" }
                    }
                }, { save: "object_name" }) // Optional - We can persist the current state information in sessionStorage.
            }
        }
    }
}
```

To use the state we simply need to import the constant declared when we created the store and pass in a selector.

```javascript
const test = useStore((state) => { return state.testState })

console.log('we access the state slice declared as testState', test)
```
